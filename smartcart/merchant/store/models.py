from django.db import models
from ...location.province.models import Province
from ...location.city.models import City
from ...location.sub_district.models import Sub_district
from ...location.village.models import Village
from ...merchant.category.models import Category
from ...merchant.models import Merchant
# Create your models here.
class  Store(models.Model):
    name = models.CharField(max_length=100)
    province = models.ForeignKey(Province, on_delete=models.SET_NULL, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)
    sub_district = models.ForeignKey(Sub_district, on_delete=models.SET_NULL, null=True, blank=True)
    village = models.ForeignKey(Village, on_delete=models.SET_NULL, null=True, blank=True)
    merchant = models.ForeignKey(Merchant, on_delete=models.SET_NULL, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    # coordinate = models.PointField(null=True, blank=True,)
    phone = models.CharField(max_length=20)
    address = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False) 
    objects = models.Manager()

    def __str__(self):
        return self.name
