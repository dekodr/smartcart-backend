from django.db import models
from .city.models import City
from .province.models import Province
from .sub_district.models import Sub_district
from .village.models import Village
# Create your models here