from django.db import models
from ..province.models import Province
from ..city.models import City
from ..sub_district.models import Sub_district
# Create your models here.
class  Village(models.Model):
    name = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=6)
    province = models.ForeignKey(Province, on_delete=models.SET_NULL, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)
    sub_district = models.ForeignKey(Sub_district, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False) 
    objects = models.Manager()
    def __str__(self):
        return self.name