# Python imports
from os.path import join

# project imports
from .common import *

# uncomment the following line to include i18n
# from .i18n import *


# ##### DEBUG CONFIGURATION ###############################
DEBUG = True

# allow all hosts during development
ALLOWED_HOSTS = ['*']

# adjust the minimal login
LOGIN_URL = 'core_login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = 'core_login'  

 
# ##### DATABASE CONFIGURATION ############################
DATABASES = {  
    'default': { 
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'smartcart',
        'USER': 'postgres',
        'PASSWORD': 'Aspire4720z',
        'HOST': 'postgres',   
        'PORT': '5432'   
    }, 
    'old': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'osmosis',
        'USER': 'coach',
        'PASSWORD': 'Aspire4720z!', 
        'HOST': '103.3.62.188',   # Or an IP Address that your DB is hosted on
        'PORT': '3306', 
    }
}

# ##### APPLICATION CONFIGURATION #########################
INSTALLED_APPS = DEFAULT_APPS
