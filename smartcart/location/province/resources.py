from import_export import resources
from .models import Province

class ProvinceResource(resources.ModelResource):
    class Meta:
        model = Province