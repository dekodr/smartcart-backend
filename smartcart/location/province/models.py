from django.db import models

# Create your models here.
class  Province(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False) 
    objects = models.Manager()
    def __str__(self):
        return self.name