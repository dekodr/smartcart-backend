from django.contrib import admin, auth, messages
from django import forms
from django.contrib.auth import hashers, models
from django.contrib.auth.hashers import make_password
from smartcart.merchant.models import Merchant
from smartcart.merchant.store.models import Store
from smartcart.merchant.cashier.models import Cashier
from smartcart.user.models import Member
from smartcart.merchant.category.models import Category
import uuid, os, string, random
from django.conf.global_settings import SECRET_KEY
from django.contrib.auth.hashers import Argon2PasswordHasher
class CashierForm(forms.ModelForm): 
    class Meta:
        exclude = ('parking_location',)
        model = Cashier 
        fields = ['code', 'store']  
        widgets = {'token': forms.HiddenInput()}  
class CashierAdmin(admin.ModelAdmin):
    # _load_library = 'Argon2PasswordHasher' 
    form = CashierForm
    readonly_fields = ('token',)
    def save_model(self, request, obj, form, change):
        # pw = models.BaseUserManager.make_random_password(self, length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
        # pw = hashers.Argon2PasswordHasher.encode(auth, password=_uuid,salt= 'test')
        def randomStringwithDigitsAndSymbols(stringLength=10):
            """Generate a random string of letters, digits and special characters """
            password_characters = string.ascii_letters + string.digits + string.punctuation
            return ''.join(random.choice(password_characters) for i in range(stringLength))
        print(request) 
        print(obj)
        print(form)
        print(change) 
        print(self)   
        if change:
            messages.info(request, "Token has not changed")
        else:
            obj.token = make_password(str(randomStringwithDigitsAndSymbols()), salt=SECRET_KEY, hasher='argon2')
            messages.info(request, "Token : "+str(randomStringwithDigitsAndSymbols()))
        
        
        super().save_model(request, obj, form, change)


# Register your models here.
admin.site.register(Merchant)
admin.site.register(Store)
admin.site.register(Member) 
admin.site.register(Cashier, CashierAdmin)
admin.site.register(Category)

 