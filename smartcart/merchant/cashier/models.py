from django.db import models
from ..store.models import Store
from django.contrib.auth.hashers import make_password
import random
import string
# Create your models here.
class  Cashier(models.Model):
    code = models.CharField(max_length=100)
    store = models.ForeignKey(Store, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False) 
    token = models.CharField(max_length=255, blank=True, null=True, editable=False) 
    objects = models.Manager()
 
    def __str__(self):  
        return self.code
 

