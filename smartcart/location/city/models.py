from django.db import models
from ..province.models import Province
# Create your models here.
class  City(models.Model):
    name = models.CharField(max_length=100)
    TYPE = (
        ("Kabupaten","Kabupaten"), ("Kota","Kota")
    )
    type = models.CharField(choices=TYPE, max_length=10)
    province = models.ForeignKey(Province, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False) 
    objects = models.Manager()
    def __str__(self):
        return self.name