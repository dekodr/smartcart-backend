from django.db import models
from .merchant.models import Merchant
from .merchant.cashier.models import Cashier
from .user.models import *
from .location.models import *
from .transaction.models import *
from .transaction.transaction_detail.models import Detail_Transaction
from django.db import connections
# Create your models here
class Migrate_data(): 
    @staticmethod
    def migrate_data(self): 
        with connections['old'].cursor() as cursor:
            cursor.execute("SELECT * FROM tr_shop_master")
            results = self.dictfetchall(self, cursor) 
            for result in results:
                n = Transaction.objects.create(
                    merchant=Merchant.objects.get(pk=1),
                    store=Store.objects.get(pk=1),
                    cashier=Cashier.objects.get(pk=1),
                    member=Member.objects.get(pk=1),
                    category=Category.objects.get(pk=1),
                    no="",
                    payment_id=result['field_3'],
                    meta=result['field_1']+";"+result['field_2'],
                    receipt_date = result['receipt_date'],
                    total=result['total'],
                    tax=result['tax'],
                    payment_method=result['payment_method'],
                    spl_file=result['spl_file']
                ) 
                print(result['id']) 
                cursor.execute("SELECT * FROM tr_shop_detail WHERE id_master = "+str(result['id']))
                results_detail = self.dictfetchall(self, cursor) 

                for result_detail in results_detail:
                    Detail_Transaction.objects.create(
                        transaction=n,
                        product = result_detail['product_name'],
                        quantity = result_detail['qty'],
                        price = result_detail['price'],
                        created_at = result_detail['entry_stamp']
                    ) 

    def dictfetchall(self, cursor): 
        "Returns all rows from a cursor as a dict" 
        desc = cursor.description 
        return [
                dict(zip([col[0] for col in desc], row)) 
                for row in cursor.fetchall() 
        ]
         