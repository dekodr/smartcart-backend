from django.db import models
from ..merchant.models import Merchant
from ..merchant.cashier.models import Cashier
from ..merchant.category.models import Category
from ..merchant.store.models import Store
from ..user.member.models import Member

class  Transaction(models.Model):
    merchant = models.ForeignKey(Merchant, on_delete=models.SET_NULL, null=True, blank=True)
    store = models.ForeignKey(Store, on_delete=models.SET_NULL, null=True, blank=True)
    cashier = models.ForeignKey(Cashier, on_delete=models.SET_NULL, null=True, blank=True)
    member = models.ForeignKey(Member, on_delete=models.SET_NULL, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    no = models.CharField(max_length=100, null=True, blank=True)
    payment_id = models.CharField(max_length=100, null=True, blank=True)
    meta = models.TextField(blank=True)
    receipt_date = models.DateTimeField()
    total = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    tax = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    payment_method = models.CharField(max_length=100, null=True, blank=True)
    spl_file = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False)
    objects = models.Manager()
