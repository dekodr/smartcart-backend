from django.urls import path

from . import views, cashierViews


app_name = "api"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('transaction', views.TransactionView.as_view()),
    path('cashier/<int:id>', cashierViews.CashierView.as_view()) 
    # path('cashier/login/', cashierViews.view()),  
]