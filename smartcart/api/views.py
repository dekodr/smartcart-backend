from django.shortcuts import render
from django.http import HttpResponse
from django.core.exceptions import ValidationError
import json
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from ..transaction.models import Transaction
from ..transaction.transaction_detail.models import Detail_Transaction
from ..merchant.cashier.models import Cashier
from ..merchant.category.models import Category
from ..merchant.store.models import Store
from ..user.member.models import Member
from ..merchant.models import Merchant
from django.db import transaction 

class TransactionView(APIView):
    def get(self, request):
        transaction = Transaction.objects.all()
        return Response(transaction)
    def post(self, request):
        transactions= request.data
        try:
            tr = Transaction.objects.create(
                merchant=Merchant.objects.get(pk=transactions['merchant_id']),
                store=Store.objects.get(pk=transactions['store_id']),
                cashier=Cashier.objects.get(pk=transactions['cashier_id']),
                member=Member.objects.get(pk=1),
                category=Category.objects.get(pk=transactions['category_id']),
                no="",
                meta=json.dumps(transactions['meta'], separators=(',', ':')),
                receipt_date = transactions['receipt_date'],
                total=transactions['total'],
                tax=transactions['tax'],
            )
            print(transactions['detail']) 
            for detail in transactions['detail']:
               
                Detail_Transaction.objects.create(
                    transaction=tr,
                    product = detail['product'],
                    quantity = detail['quantity'], 
                    price = detail['price']
                ) 
            return Response({"status":"Success"}, status=status.HTTP_201_CREATED)
        except ValidationError as e:
            return Response({"status":e}, status=status.HTTP_406_NOT_ACCEPTABLE)
            

        
