from django.db import models
from ..models import Transaction
class  Detail_Transaction(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.SET_NULL, null=True, blank=True)
    product = models.CharField(max_length=100)
    quantity = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False)
    objects = models.Manager()