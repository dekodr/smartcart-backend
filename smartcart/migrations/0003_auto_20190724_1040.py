# Generated by Django 2.2.3 on 2019-07-24 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smartcart', '0002_auto_20190714_1914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='payment_id',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
