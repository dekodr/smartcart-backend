from django.shortcuts import render
from django.http import HttpResponse
import csv
from .location.models import *
from .models import Migrate_data
# Create your views here.

def migrate_data(request):
    Migrate_data.migrate_data(Migrate_data)
    return HttpResponse("")
 
def import_area(request):
    with open('./static/data_wilayah.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=",")
        for row in readCSV:
            if row:
                province = Province.objects.filter(name = row[1])
                if province.count() == 0:
                    Province.objects.create(name = row[1])
                id_province = Province.objects.get(name=row[1])
                
                if id_province:
                    city = City.objects.filter(name=row[3], type = row[2])
                    if city.count() == 0:
                        City.objects.create(name = row[3], type = row[2], id_province = id_province)
                    id_city = City.objects.get(name=row[3], type = row[2], id_province = id_province)
                    if id_city:
                        sub_district = Sub_district.objects.filter(name=row[4], id_province= id_province, id_city=id_city)
                        if sub_district.count() == 0:
                            Sub_district.objects.create(name = row[4], id_province = id_province, id_city=id_city)
                        id_sub_district = Sub_district.objects.get(name=row[4], id_province = id_province, id_city=id_city)
                        if id_sub_district:
                            village = Village.objects.filter(
                                name=row[5], 
                                postal_code=row[6],
                                id_province= id_province, 
                                id_city=id_city, 
                                id_sub_district=id_sub_district
                            )
                            if village.count() == 0:
                                Village.objects.create(
                                    name=row[5],  
                                    postal_code=row[6],
                                    id_province= id_province, 
                                    id_city=id_city, 
                                    id_sub_district=id_sub_district 
                                ) 
                        
                
    return ""
