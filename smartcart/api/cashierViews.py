from django.shortcuts import render
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from django.core import serializers
import json, base64
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from ..merchant.cashier.models import Cashier
from django.db import transaction

class CashierView(APIView): 
    def get(self, request, id):
        cashier = Cashier.objects
        data = cashier.select_related('store').get(id=id)  
        province_code = "{:02d}".format(data.store.province_id)  
        city_code = "{:03d}".format(data.store.city_id)
        pos_code = "{:06d}".format(data.id)
        merchant_code = "{:03d}".format(data.store.merchant_id)
        store_code = "{:06d}".format(data.store.id) 
        terminal_id = province_code+city_code+merchant_code+store_code+pos_code
        result_data = {
            "store_id": data.store.id,
            "province_name": data.store.province.name,
            "city_name": data.store.city.name,
            "address": data.store.address,
            "cashier_id":id,
            "terminal_id":terminal_id,
            "store_name":data.store.name
        }   
        return HttpResponse( 
            json.dumps(result_data),
            content_type="application/json"
        ) 